from django.contrib import admin
from django.urls import path


urlpatterns = [
    path('admin/', admin.site.urls),
    # path('swot_analyzer/', 'apps.swot_analyzer.analyzer.urls'),
]
