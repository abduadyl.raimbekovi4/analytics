from django.apps import AppConfig


class SwotAnalyzerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.swot_analyzer'

    def ready(self) -> None:
        self._setup_di()
        self._setup_signals()

    def _setup_di(self) -> None:
        from apps.swot_analyzer.container import SWOTAnalyzerContainer

        container = SWOTAnalyzerContainer()
        container.wire(
            modules=[]
        )

    def _setup_signals(self) -> None:
        pass
