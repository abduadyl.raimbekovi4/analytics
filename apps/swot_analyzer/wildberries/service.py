import logging

from typing import Optional

from apps.swot_analyzer.wildberries.api_client import WildberriesAPIClient
from apps.swot_analyzer.wildberries.types import (
    CategoryURI,
    WBCategoryDTO,
    WBProductDTO,
    WBProductSalesDTO,
    WBProductReviewDTO,
    ProductSKU,
    ProductReviewStar,
)
from apps.swot_analyzer.utils.exceptions import ServiceException


logger = logging.getLogger(__name__)


class WildberriesServiceError(ServiceException):
    class Code:
        REVIEWS_NOT_FOUND = 'Reviews of product not found'


class WildberriesService:
    def __init__(
            self,
            config: dict,
            wb_api_client: WildberriesAPIClient,
    ) -> None:
        self._config = config
        self._wb_api_client = wb_api_client

    def get_products_from_category(
            self,
            category_uri: CategoryURI,
            pages_count: int,
    ) -> list[WBProductDTO]:
        """
        Get products from category by category link and num pages
        """
        category: WBCategoryDTO = self._wb_api_client.get_category(category_uri=category_uri)

        extracted_products: list[WBProductDTO] = []

        # Start page always starts from 1
        __start_page: int = 1

        # Need to refactor this shit :) n+1
        # Async
        for page in range(__start_page, pages_count):
            products: list[WBProductDTO] = self._wb_api_client.get_products_from_category_page(
                category=category, page=page
            )
            extracted_products.extend(products)
        return extracted_products

    def get_top_competitors_by_sales(
            self,
            products: list[WBProductDTO],
            num_of_competitors: int,
    ) -> list[WBProductDTO]:
        """
        Get top competitors by order quantity
        """
        sales: list[WBProductSalesDTO] = self.get_products_sales(products=products)
        sorted_by_sales: list[WBProductSalesDTO] = sorted(sales, key=lambda x: x.sales, reverse=True)
        top_sales_sku: list[ProductSKU] = [sale.product_sku
                                           for sale in sorted_by_sales[:num_of_competitors]]
        top_competitors: list[WBProductDTO] = [product
                                               for product in products
                                               if product.sku in top_sales_sku]
        return top_competitors

    def get_sorted_reviews_by_star(
            self,
            product: WBProductDTO,
    ) -> dict[ProductReviewStar, list[WBProductReviewDTO]]:
        """
        Sorted reviews by star to dict
        """
        reviews: list[WBProductReviewDTO] = self._wb_api_client.get_product_reviews(
            product_root=product.root
        )

        if not reviews:
            logger.exception(WildberriesServiceError.Code.REVIEWS_NOT_FOUND)

        reviews_by_star: dict[ProductReviewStar, list[WBProductReviewDTO]] = {
            ProductReviewStar(1): [],
            ProductReviewStar(2): [],
            ProductReviewStar(3): [],
            ProductReviewStar(4): [],
            ProductReviewStar(5): [],
        }

        for review in reviews:
            star: ProductReviewStar = review.star
            reviews_by_star[star].append(review)

        return reviews_by_star

    def get_products_sales(
            self,
            products: list[WBProductDTO]
    ) -> list[WBProductSalesDTO]:
        """
        Get orders quantity of product
        """
        extracted_products_sales: list[WBProductSalesDTO] = []

        # Need to refactor this shit :) n+1
        # Async
        for product in products:
            sales: Optional[WBProductSalesDTO] = self._wb_api_client.get_product_orders_quantity(product.sku)
            extracted_products_sales.append(sales)
        return extracted_products_sales
