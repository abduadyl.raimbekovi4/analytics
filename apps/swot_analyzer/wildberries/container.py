from dependency_injector.containers import DeclarativeContainer
from dependency_injector import providers

from apps.swot_analyzer.wildberries.api_client import WildberriesAPIClient
from apps.swot_analyzer.wildberries.service import WildberriesService


class WildberriesContainer(DeclarativeContainer):
    config = providers.Configuration()

    wb_api_client: providers.Dependency[WildberriesAPIClient] = providers.Singleton(
        WildberriesAPIClient,
        config=config,
    )
    wb_service: providers.Dependency[WildberriesService] = providers.Singleton(
        WildberriesService,
        config=config,
        wb_api_client=wb_api_client,
    )
