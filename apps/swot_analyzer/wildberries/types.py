from dataclasses import dataclass
from typing import NewType


ProductSKU = NewType('ProductSKU', int)
ProductRoot = NewType('ProductRoot', int)
ProductURI = NewType('ProductURI', str)
CategoryURI = NewType('CategoryURI', str)
ProductSales = NewType('ProductSales', int)
ProductReviewStar = NewType('ProductReviewStar', int)


@dataclass(frozen=True)
class WBCategoryDTO:
    name: str
    url: str
    shard: str | None
    query: str


@dataclass(frozen=True)
class WBProductDTO:
    root: ProductRoot
    product_uri: ProductURI
    title: str
    sku: ProductSKU
    brand: str
    brand_id: int
    price: int
    discounted_price: int
    rating: int
    reviews: int
    sales: ProductSales | None


@dataclass(frozen=True)
class WBProductReviewDTO:
    star: ProductReviewStar
    text: str


@dataclass(frozen=True)
class WBProductSalesDTO:
    product_sku: ProductSKU
    sales: int
