import requests
import logging

from typing import Optional

from requests.exceptions import RequestException

from apps.swot_analyzer.utils.exceptions import HTTPException, APIClientException
from apps.swot_analyzer.wildberries.types import (
    WBProductDTO,
    WBCategoryDTO,
    WBProductReviewDTO,
    WBProductSalesDTO,
    ProductSKU,
    ProductRoot,
    ProductURI,
    CategoryURI,
)


logger = logging.getLogger(__name__)


class WildberriesAPIClientError(APIClientException):
    class Code:
        CATEGORY_NOT_FOUND = 'Category not found'
        PAGE_PRODUCTS_NOT_FOUND = 'Page of products not found'


class WildberriesAPIClient:
    def __init__(
            self,
            config: dict,
    ) -> None:
        self._config = config
        self._user_agent_headers = {
            'Accept': "*/*",
            'User-Agent': "Chrome/51.0.2704.103 Safari/537.36",
        }
        self._base_url = config['base_url']

    def get_product_detail(self) -> WBProductDTO:
        pass

    def get_product_orders_quantity(
            self,
            product_sku: ProductSKU
    ) -> Optional[WBProductSalesDTO]:
        """
        Get orders quantity from Product by article (Product -> sku)
        """
        url: str = f"https://product-order-qnt.wildberries.ru/by-nm/?nm={product_sku}"
        product_orders_quantity: dict = self._request(url=url)

        if not product_orders_quantity:
            logger.debug(f'Orders of product not found sku={product_sku}')
            sales: dict = {
                'nmId': product_sku,
                'qnt': 0
            }
            return self._build_sales_dto(sales=sales)

        sales: dict = product_orders_quantity[0]
        return self._build_sales_dto(sales=sales)

    def get_product_reviews(
            self,
            product_root: ProductRoot
    ) -> list[WBProductReviewDTO]:
        """
        Get reviews of product by product root
        """
        _start_bucket_number = 1
        _end_bucket_number = 2

        reviews: list[WBProductReviewDTO] = []

        for bucket_number in range(_start_bucket_number, _end_bucket_number + 1):
            url: str = f'https://feedbacks{bucket_number}.wb.ru/feedbacks/v1/{product_root}'
            reviews_data: dict = self._request(url=url)

            if reviews_data['feedbacks']:
                reviews.extend([self._build_review_dto(review) for review in reviews_data['feedbacks']])
                break  # Exit the loop if reviews are found

        if not reviews:
            logger.debug(f'Reviews of product not found root={product_root}')
            
        return reviews

    def get_products_from_category_page(
            self,
            category: WBCategoryDTO,
            page: int
    ) -> list[WBProductDTO]:
        """
        Get products from page category
        """
        url: str = f"https://catalog.wb.ru/catalog/{category.shard}/catalog" \
                   f"?appType=1&{category.query}&curr=rub&dest=-1257786&page={page}&sort=popular&spp=24"
        page_data: dict = self._request(url=url)

        if not page:
            logger.exception(f'Page of products not found with given url {url}')
            raise WildberriesAPIClientError(WildberriesAPIClientError.Code.PAGE_PRODUCTS_NOT_FOUND)

        return [
            self._build_product_dto(product)
            for product in page_data['data']['products']
        ]

    def get_category(
            self,
            category_uri: CategoryURI
    ) -> WBCategoryDTO:
        """
        Get category
        """
        url: str = 'https://static-basket-01.wb.ru/vol0/data/main-menu-ru-ru-v2.json'
        categories_data: dict = self._request(url=url, params={})

        flattened_categories: list = []
        self._traverse_categories(categories=categories_data, flattened_categories=flattened_categories)

        category = next(
            (
                WBCategoryDTO(
                    name=category['name'],
                    shard=category['shard'],
                    query=category['query'],
                    url=category['url'],
                )
                for category in flattened_categories
                if category_uri.split(self._base_url)[-1] == category['url']
            ),
            None,
        )

        if not category:
            logger.exception(f'Category not found with given link {category_uri}')
            raise WildberriesAPIClientError(WildberriesAPIClientError.Code.CATEGORY_NOT_FOUND)
        return category

    def _traverse_categories(
            self,
            categories,
            flattened_categories: list
    ) -> None:
        """
        Recursively traverse the JSON categories and flatten it to a list.
        """
        for category in categories:
            try:
                flattened_categories.append({
                    'name': category['name'],
                    'url': category['url'],
                    'shard': category['shard'],
                    'query': category['query']
                })
            except KeyError as e:
                logger.error(f'Missing key {e} in category: {category["name"]}')
                continue

            if 'childs' in category:
                self._traverse_categories(category['childs'], flattened_categories)

    def _build_product_dto(self, product: dict) -> WBProductDTO:
        return WBProductDTO(
            root=product['root'],
            product_uri=ProductURI(f'{self._base_url}/catalog/{product["id"]}/detail.aspx'),
            title=product['name'],
            sku=product['id'],
            brand=product['brand'],
            brand_id=product['brandId'],
            price=int(product['priceU'] / 100),
            discounted_price=int(product['salePriceU'] / 100),
            rating=product['rating'],
            reviews=product['feedbacks'],
            sales=None,
        )

    def _build_review_dto(self, review: dict) -> WBProductReviewDTO:
        return WBProductReviewDTO(
            star=review['productValuation'],
            text=review['text'],
        )

    def _build_sales_dto(self, sales: dict) -> WBProductSalesDTO:
        return WBProductSalesDTO(
            product_sku=sales['nmId'],
            sales=sales['qnt'],
        )

    def _request(
            self,
            url: str,
            params: dict | None = None
    ) -> dict:
        try:
            response = requests.get(
                url=url,
                headers=self._user_agent_headers,
                params=params
            )
        except RequestException as e:
            logger.exception(f'GET request to {url} with params {params} failed: {e}')
            raise HTTPException(e) from e
        return response.json()
