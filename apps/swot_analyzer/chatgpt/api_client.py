import logging

from openai.types.chat import ChatCompletion
from openai import AsyncOpenAI


logger = logging.getLogger(__name__)


class ChatGPTApiClient:
    def __init__(
            self,
            config: dict,
    ) -> None:
        self._config = config
        self._gpt_model = config['gpt_model']
        self._client = AsyncOpenAI(api_key=config['api_key'])

    async def create_chat_completion(
            self,
            messages: list[dict]
    ) -> ChatCompletion:
        return await self._client.chat.completions.create(
            messages=messages,
            model=self._gpt_model,
        )
