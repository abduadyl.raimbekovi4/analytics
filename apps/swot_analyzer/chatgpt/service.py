import logging
import asyncio

from openai.types.chat import ChatCompletion

from apps.swot_analyzer.chatgpt.api_client import ChatGPTApiClient


logger = logging.getLogger(__name__)


class ChatGPTService:
    def __init__(
            self,
            config: dict,
            chatgpt_api_client: ChatGPTApiClient,
    ) -> None:
        self._config = config
        self._chatgpt_api_client = chatgpt_api_client

    def get_message_from_assistant_swot_analyzer(
            self,
            content: str,
    ) -> dict:
        """
        GPT SWOT Analyzer
        """
        messages = [
            {
                "role": "system",
                "content": "You are a product analyst, who is responsible for creating a SWO "
                           "(Strength, Weakness, and Opportunity) analysis of the product. "
                           "In Strength analysis, I want you to summarize the most positive aspects of the product,"
                           "including the variations, colors, material, and overall strength of the product. In "
                           "Weakness analysis, I want you to summarize all the bad reviews, and tell me where "
                           "product is lacking, and what features of the product are the worst ones. In Opportunity"
                           "analysis, I want you to tell me how to improve this product, by giving my all "
                           "the possible improvements and solutions to beat the product."
            },
            {
                "role": "user",
                "content": content,
            }
        ]
        chat_completion: ChatCompletion = asyncio.run(
            self._chatgpt_api_client.create_chat_completion(messages=messages)
        )

        return {
            "message": chat_completion.choices[0].message
        }
