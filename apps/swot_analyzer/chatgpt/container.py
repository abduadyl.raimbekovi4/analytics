from dependency_injector.containers import DeclarativeContainer
from dependency_injector import providers

from apps.swot_analyzer.chatgpt.service import ChatGPTService
from apps.swot_analyzer.chatgpt.api_client import ChatGPTApiClient


class ChatGPTContainer(DeclarativeContainer):
    config = providers.Configuration()

    chatgpt_api_client: providers.Dependency[ChatGPTApiClient] = providers.Singleton(
        ChatGPTApiClient,
        config=config,
    )

    chatgpt_service: providers.Dependency[ChatGPTService] = providers.Singleton(
        ChatGPTService,
        config=config,
        chatgpt_api_client=chatgpt_api_client,
    )
