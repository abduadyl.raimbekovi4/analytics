from django.urls import path

from .views import SWOTAnalyzerView


urlpatterns = [
    path('process_analyze/', SWOTAnalyzerView.as_view()),
]
