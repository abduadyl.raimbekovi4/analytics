from dependency_injector.containers import DeclarativeContainer
from dependency_injector import providers

from apps.swot_analyzer.wildberries.service import WildberriesService
from apps.swot_analyzer.chatgpt.service import ChatGPTService
from apps.swot_analyzer.analyzer.service import AnalyzerService


class AnalyzerContainer(DeclarativeContainer):
    config = providers.Configuration()

    wb_service: providers.Dependency[WildberriesService] = providers.Dependency()
    chatgpt_service: providers.Dependency[ChatGPTService] = providers.Dependency()

    analyzer_service: providers.Dependency[AnalyzerService] = providers.Singleton(
        AnalyzerService,
        config=config,
        wb_service=wb_service,
        chatgpt_service=chatgpt_service,
    )
