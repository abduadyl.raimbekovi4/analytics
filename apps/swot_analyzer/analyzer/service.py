import logging

from apps.swot_analyzer.wildberries.service import WildberriesService
from apps.swot_analyzer.chatgpt.service import ChatGPTService
from apps.swot_analyzer.wildberries.types import (
    CategoryURI,
    WBProductDTO,
    WBProductReviewDTO,
    ProductReviewStar,
)


logger = logging.getLogger(__name__)


class AnalyzerService:
    def __init__(
            self,
            config: dict,
            wb_service: WildberriesService,
            chatgpt_service: ChatGPTService,
    ) -> None:
        self._config = config
        self._wb_service = wb_service
        self._chatgpt_service = chatgpt_service

    def prepare_content_for_analyzer(
            self,
            product: WBProductDTO,
            reviews: list[WBProductReviewDTO]
    ) -> str:
        product_info = f"Product: {product.title}\nBrand: {product.brand}\nPrice: {product.price}\n"

        # Concatenate all review texts
        review_text = "\n".join([f"Review {i + 1}:\n{review.text}" for i, review in enumerate(reviews)])

        return f"{product_info}\nReviews:\n{review_text}"

    def process_swot_analyzer(
            self,
            category_uri: CategoryURI,
    ):
        products: list[WBProductDTO] = self._wb_service.get_products_from_category(
            category_uri=category_uri, pages_count=2
        )
        top_competitors: list[WBProductDTO] = self._wb_service.get_top_competitors_by_sales(
            products=products, num_of_competitors=5
        )

        for product in top_competitors:
            reviews: dict[ProductReviewStar, list[WBProductReviewDTO]] = \
                self._wb_service.get_sorted_reviews_by_star(product=product)
            content = self.prepare_content_for_analyzer(product=product, reviews=reviews[ProductReviewStar(1)])
            response = self._chatgpt_service.get_message_from_assistant_swot_analyzer(content=content)
