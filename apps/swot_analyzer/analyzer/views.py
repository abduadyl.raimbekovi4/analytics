from typing import Any
from dataclasses import asdict

from rest_framework import views
from rest_framework.response import Response

from dependency_injector.wiring import inject, Provide

from apps.swot_analyzer.analyzer.service import AnalyzerService
from apps.swot_analyzer.container import SWOTAnalyzerContainer


class SWOTAnalyzerView(views.APIView):

    @inject
    def __init__(
            self,
            analyzer_service: AnalyzerService = Provide[SWOTAnalyzerContainer.analyzer_package.analyzer_service],
            **kwargs: Any,
    ):
        self._analyzer_service = analyzer_service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        self._analyzer_service.process_swot_analyzer()
