

class ServiceException(Exception):
    pass


class HTTPException(Exception):
    pass


class APIClientException(Exception):
    pass
