from dependency_injector import containers, providers

from apps.swot_analyzer.wildberries.container import WildberriesContainer
from apps.swot_analyzer.analyzer.container import AnalyzerContainer
from apps.swot_analyzer.chatgpt.container import ChatGPTContainer
from apps.swot_analyzer.logger.container import LoggerContainer


def get_config() -> list:
    return ['config.yaml']


class SWOTAnalyzerContainer(containers.DeclarativeContainer):
    config = providers.Configuration(yaml_files=get_config(), strict=True)

    logger_package = providers.Container(
        LoggerContainer,
        config=config.logger,
    )
    wb_package = providers.Container(
        WildberriesContainer,
        config=config.wildberries,
    )
    chatgpt_package = providers.Container(
        ChatGPTContainer,
        config=config.chatgpt,
    )
    analyzer_package = providers.Container(
        AnalyzerContainer,
        config=config.analyzer,
        wb_service=wb_package.wb_service,
        chatgpt_service=chatgpt_package.chatgpt_service,
    )
