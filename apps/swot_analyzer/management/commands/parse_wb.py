from django.core.management.base import BaseCommand, CommandError
from apps.swot_analyzer.wildberries.api_client import WildberriesAPIClient
from apps.swot_analyzer.analyzer.service import AnalyzerService
from apps.swot_analyzer.wildberries.service import WildberriesService
from apps.swot_analyzer.chatgpt.service import ChatGPTService
from apps.swot_analyzer.chatgpt.api_client import ChatGPTApiClient


class Command(BaseCommand):
    help = 'Parsing WB for testing'

    def handle(self, *args, **options):

        wb_api_client = WildberriesAPIClient(config={"base_url": "https://www.wildberries.ru"})
        wb_service = WildberriesService(config={}, wb_api_client=wb_api_client)
        chatgpt_api_client = ChatGPTApiClient(config={'gpt_model': 'gpt-3.5-turbo',
                                                      'api_key': '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'})
        chatgpt_service = ChatGPTService(config={}, chatgpt_api_client=chatgpt_api_client)

        analyzer_service = AnalyzerService(config={}, wb_service=wb_service, chatgpt_service=chatgpt_service)

        analyzer_service.process_swot_analyzer(
            category_uri="https://www.wildberries.ru/catalog/zhenshchinam/odezhda/bryuki-i-shorty"
        )

        # wb_api_client.get_products_from_category(
        #     'https://www.wildberries.ru/catalog/muzhchinam/odezhda/dzhempery-i-kardigany'
        # )
        # wb_api_client.get_product_reviews(product_root=162248751)
        # WBProductDataObject(root=162248751, product_uri='https://www.wildberries.ru/catalog/50832112/detail.aspx',
        #                     title='Солнцезащитный крем для лица спф 20', sku=50832112, brand='ISRAELIK',
        #                     brand_id=1065307, price=3690, discounted_price=777, rating=5, reviews=3818, sales=None)