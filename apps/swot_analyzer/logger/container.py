import logging.config

from dependency_injector.containers import DeclarativeContainer
from dependency_injector import providers


class LoggerContainer(DeclarativeContainer):
    config = providers.Configuration()

    logging = providers.Resource(
        logging.config.dictConfig,
        config=config.logging,
    )


